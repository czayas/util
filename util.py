#!/usr/bin/env python
#-*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Módulo: Util - Version 1.12 - 11/11/2011 (Rev. 04/09/2013)
# Carlos Zayas Guggiari <carlos@zayas.org>
# python --version : Python 2.7.4
# -----------------------------------------------------------------------------

'''
Funciones, procedimientos y clases útiles:

ARCHIVO
fun archivos(ruta='.', ext='')
pro borrarsiexiste(archivo)
pro cambiarcarpeta(direccion)
fun caminoabsoluto(direccion)
fun caminoactual()
fun carpetapersonal()
pro crearcarpeta(direccion)
fun esarchivo(cadena)
fun escarpeta(cadena)
fun existe(cadena)
fun fixpath(cadena)
fun guardar(lista, nombre)
fun leer(archivo)
fun listarcarpeta(direccion)
fun montajes()
cla Base(nombre=name()+'.db')

CADENAS
fun ancho(lista)
pro borrar(cantidad)
pro borrarlinea()
pro clear(l=100)
fun columnas(lista, cols, sep=None)
fun escadena(objeto)
fun eslista(objeto)
fun esbinario(dato)
fun espacios(cantidad)
fun juntar(lista, sep='\\n')
fun limpiar(cadena)
fun llenar(cadena, cantidad, caracter='0', ajuste='d')
pro mostrar(objeto, ordenar=False)
pro out(cadena)
fun cat(*items)
fun tupla(lista, comillas=False)

INGRESO
fun esc()
fun getkey()
fun inp(mensaje='', longitud=40, permitidas=IMPRIMIBLES, sep='')
cla Consola(prompt='> ', intro="¡Bienvenido a " + name() + "!")

INTROSPECCION
fun doc(objeto)
fun name()
fun tipo(objeto)

SISTEMA
fun arg(pos=False)
fun cursor(mostrar=True)
fun ejecutar(c)
fun fecha(epoch, corta=True)
fun fechas()
fun hostname()
fun xy()
pro error(mensaje)
pro salir(estado=0)

LAMBDAS
miles : Devuelve una cadena con el número separado en miles.
esbin : Devuelve True si los bytes no son todos caracteres de texto.
veces : Devuelve una lista en base a un rango, de 1 al número especificado.
'''

# -----------------------------------------------------------------------------
# Importación de módulos
# -----------------------------------------------------------------------------

import os, sys, string, datetime, cmd, readline, sqlite3, socket, re

from subprocess import Popen, PIPE, STDOUT

reload(sys)

sys.setdefaultencoding('utf-8')

# -----------------------------------------------------------------------------
# Constantes
# -----------------------------------------------------------------------------

ESC = chr(27)  # Escape
SPC = chr(32)  # Espacio
BKS = chr(127) # Backspace ('\b')
TAB = '\t'     # Tab Horizontal
LF  = '\n'     # New Line Feed
CR  = '\r'     # Carriage Return

ESC_CLRLINE = ESC + '[2K' # Borrar la línea completa
ESC_CLRTEOL = ESC + '[K'  # Borrar hasta fin de línea
ESC_CSRHOME = ESC + '[u'  # Mover cursor al principio de la línea
ESC_CSRBACK = ESC + '[D'  # Mover cursor un espacio atrás

CSLT = ESC + chr(91) + chr(68) # ESC [D
CSRT = ESC + chr(91) + chr(67) # ESC [C
CSUP = ESC + chr(91) + chr(65) # ESC [A
CSDN = ESC + chr(91) + chr(66) # ESC [B

CSON = '\033[?25h' # Cursor ON  (Muestra el cursor)
CSOF = '\033[?25l' # Cursor OFF (Oculta el cursor)

KEY_INS = chr(27) + chr(91) + chr(50) + chr(126)
KEY_DEL = chr(27) + chr(91) + chr(51) + chr(126)
KEY_HOM = chr(27) + chr(79) + chr(72)
KEY_END = chr(27) + chr(79) + chr(70)
KEY_PUP = chr(27) + chr(91) + chr(53) + chr(126)
KEY_PDN = chr(27) + chr(91) + chr(54) + chr(126)

BORRARLINEA = CSUP + ESC_CLRLINE + CSUP + ESC_CLRLINE

IMPRIMIBLES = string.digits + string.letters + string.punctuation + SPC

TEXTCHARS = ''.join(map(chr, [7,8,9,10,12,13,27] + range(0x20, 0x100)))

diasemana = {'MONDAY':'Lunes','TUESDAY':'Martes','WEDNESDAY':'Miercoles',
             'THURSDAY':'Jueves','FRIDAY':'Viernes','SATURDAY':'Sabado',
             'SUNDAY':'Domingo'}

mes = {'JANUARY':'Enero','FEBRUARY':'Febrero','MARCH':'Marzo','APRIL':'Abril',
       'MAY':'Mayo','JUNE':'Junio','JULY':'Julio','AUGUST':'Agosto',
       'SEPTEMBER':'Setiembre','OCTOBER':'Octubre','NOVEMBER':'Noviembre',
       'DECEMBER':'Diciembre'}

# -----------------------------------------------------------------------------
# Lambdas
# -----------------------------------------------------------------------------

miles = lambda x: (".".join(re.findall("\d{1,3}",str(x)[::-1])))[::-1] # step-1

esbin = lambda bytes: bool(bytes.translate(None, TEXTCHARS))

veces = lambda x: range(1, x + 1)

# -----------------------------------------------------------------------------
# Funciones
# -----------------------------------------------------------------------------

def arg(pos=False):

    'Retorna un parámetro o los parámetros de la línea de comandos.'

    if len(sys.argv) > 1:
        salida = sys.argv[pos] if pos else sys.argv[1:]
    else:
        salida = []

    return salida

# -----------------------------------------------------------------------------

def hostname():

    'Retorna el nombre del equipo.'

    return socket.gethostname()

# -----------------------------------------------------------------------------

def xy():

    'Retorna una tupla con las dimensiones de la consola: (x,y)'

    filas, columnas = os.popen('stty size', 'r').read().split()

    return int(columnas), int(filas)

# -----------------------------------------------------------------------------

def existe(cadena):

    'Retorna verdadero si la trayectoria es correcta.'

    return os.path.exists(cadena)

# -----------------------------------------------------------------------------

def esarchivo(cadena):

    'Retorna verdadero si la trayectoria corresponde a un archivo.'

    return os.path.isfile(cadena)

# -----------------------------------------------------------------------------

def escarpeta(cadena):

    'Retorna verdadero si la trayectoria corresponde a una carpeta.'

    return os.path.isdir(cadena)

#------------------------------------------------------------------------------

def escadena(objeto):

    'Retorna verdadero si el objeto pasado es una cadena (str o unicode).'

    return isinstance(objeto, basestring)

#------------------------------------------------------------------------------

def eslista(objeto):

    'Retorna verdadero si el objeto pasado es una lista o tupla.'

    return type(objeto).__name__ in ('list','tuple')

#------------------------------------------------------------------------------

def esbinario(dato):

    'Retorna verdadero si el dato es binario (tiene bytes no imprimibles).'

    binario = False

    if len(dato)>1024:
        bytes = dato[:1024]
    else:
        bytes = dato[:]

    for byte in bytes:
        if byte not in TEXTCHARS:
            binario = True
            break

    return binario

#------------------------------------------------------------------------------

def leer(archivo):

    'Retorna una lista con el contenido del archivo especificado.'

    return open(archivo).readlines()

# -----------------------------------------------------------------------------

def caminoabsoluto(direccion):

    'Retorna el camino absoluto a una carpeta.'

    return os.path.abspath(direccion)

# -----------------------------------------------------------------------------

def caminoactual():

    'Retorna la carpeta de trabajo actual.'

    return os.getcwd()

# -----------------------------------------------------------------------------

def carpetapersonal():

    'Retorna la carpeta personal del usuario actual.'

    return os.path.expanduser('~')

# -----------------------------------------------------------------------------

def listarcarpeta(direccion):

    'Retorna el contenido de una carpeta.'

    return os.listdir(direccion)

# -----------------------------------------------------------------------------

def tipo(objeto):

    'Retorna el tipo de un objeto o trayectoria.'

    nombre = type(objeto).__name__

    if nombre == 'str':

        if os.path.isdir(objeto):
            nombre = 'dir'
        elif os.path.isfile(objeto):
            nombre = 'file'
        elif os.path.islink(objeto):
            nombre = 'link'
        elif os.path.ismount(objeto):
            nombre = 'mount'

    return nombre

# -----------------------------------------------------------------------------

def archivos(ruta='.', ext=''):

    'Retorna los archivos de una ruta y extensión específicas.'

    for root,dirs,files in os.walk(ruta):
        for archivo in [f for f in files if f.lower().endswith(ext)]:
            archivo = unicode(archivo)
            yield os.path.join(root, archivo)

# -----------------------------------------------------------------------------

def fixpath(cadena):

    'Retorna una trayectoria con caracteres especiales corregidos.'

    fixchar = {'\\040':' '} # Hasta ahora, sólo tuve problemas con un caracter.

    for char in fixchar:
        if char in cadena:
            cadena = cadena.replace(char, fixchar[char])

    return cadena

# -----------------------------------------------------------------------------

def montajes():

    'Retorna un diccionario de volúmenes montados.'

    lineas = ejecutar('mount -v') # -v : verbose

    devices = map(lambda l: l.split(' on ')[0], lineas)
    mpoints = map(lambda l: l.split(' on ')[1].split(' type ')[0], lineas)

    return dict(zip(devices,mpoints))

#------------------------------------------------------------------------------

def fecha(epoch, corta=True):

    'Devuelve la fecha y la hora convertidas desde el formato Unix Epoch.'

    dia = {'MON':'Lun','TUE':'Mar','WED':'Mie','THU':'Jue','FRI':'Vie',       \
           'SAT':'Sab','SUN':'Dom'}

    mes = {'JAN':'Ene','FEB':'Feb','MAR':'Mar','APR':'Abr','MAY':'May',       \
           'JUN':'Jun','JUL':'Jul','AUG':'Ago','SEP':'Set','OCT':'Oct',       \
           'NOV':'Nov','DEC':'Dic'}

    formato = "%a, %d %b %Y %H:%M:%S %Z" # Ej: Mar, 14 Feb 2012 21:54:59 PYST

    if corta: formato = "%d %b %Y %H:%M" # Ej: 14 Feb 2012 21:54

    f = time.strftime(formato, time.localtime(epoch)).upper()

    if corta:
        m = f[3:6]               # Mes (en inglés)
    else:
        m = f[8:11]              # Mes (en inglés)
        d = f[:3]                # Día (en inglés)
        f = f.replace(d, dia[d]) # Traduce día al castellano.

    f = f.replace(m, mes[m])     # Traduce mes al castellano.

    return f

# -----------------------------------------------------------------------------

def fechas():

    '''
    Retorna un diccionario con las siguientes claves:

    fecha     : Fecha actual en formato 'datetime.date'
    fechora   : Fecha y hora actuales en formato 'datetime.date'
    hora      : Hora actual en formato 'str'
    dia       : Dia del mes actual en formato 'str'
    mes       : Mes actual en formato 'str'
    ano       : Año actual en formato 'str'
    nombredia : Dia de la semana segun diccionario en formato 'str'
    nombremes : Mes actual segun diccionario en formato 'str'
    '''

    # d = fechas()
    # print 'Son las %s del %s %s de %s de %s' % \
    #       (d['hora'],d['dia'],d['fecha'].day,d['mes'],d['fecha'].year)

    dic = {}
    dic['fecha']     = datetime.date.today()
    dic['fechora']   = datetime.datetime.today()
    dic['hora']      = dic['fechora'].time().__str__().split('.')[0]
    dic['dia']       = dic['fecha'].strftime('%d')
    dic['mes']       = dic['fecha'].strftime('%m')
    dic['ano']       = dic['fecha'].strftime('%Y')
    dic['nombredia'] = diasemana[dic['fecha'].strftime('%A').upper()]
    dic['nombremes'] = mes[dic['fecha'].strftime('%B').upper()]

    return dic

# -----------------------------------------------------------------------------

def doc(objeto):

    'Retorna una lista con la documentación completa de un objeto.'

    documentacion = []

    for item in dir(objeto):

        documentacion.append([item, eval('objeto.' + item + '.__doc__')])

    return documentacion

# -----------------------------------------------------------------------------

def limpiar(cadena):

    'Quita espacios y newlines del final de una cadena.'

    while cadena and (cadena[-1] in [SPC,LF]):

        cadena = cadena[:-1]

    return cadena

# -----------------------------------------------------------------------------

def espacios(cantidad):

    'Retorna una cadena de espacios.'

    return SPC * cantidad

# -----------------------------------------------------------------------------

def ancho(lista):

    'Retorna la longitud del item más largo de una lista.'

    longitud = 0

    for item in lista:
        if len(item) > longitud:
            longitud = len(item)

    return longitud

# -----------------------------------------------------------------------------

def llenar(cadena, cantidad, caracter='0', ajuste='d'):

    'Retorna una cadena de ancho fijo ajustada con caracteres de relleno.'

    if len(cadena) >= cantidad:
        salida = cadena
    else:
        relleno = caracter * (cantidad - len(cadena))
        if ajuste == 'i':
            salida = cadena + relleno
        elif ajuste == 'd':
            salida = relleno + cadena
        else:
            salida = 'ERROR'

    return salida

# -----------------------------------------------------------------------------

def juntar(lista, sep='\n'):

    'Retorna una cadena de caracteres en base a una lista.'

    cadena = ''

    for item in lista:
        cadena += str(item) + sep

    return cadena[:-len(sep)]

# -----------------------------------------------------------------------------

def esc():

    'Gestiona el empleo de las teclas con códigos Escape.'

    a = [0,0,0,0]

    a[0] = ord(getkey())

    if  a[0] == 91 or  a[0] == 79:  a[1] = ord(getkey())
    if (a[1] >= 49 and a[1] <= 54): a[2] = ord(getkey())
    if (a[2] >= 48 and a[2] <= 57): a[3] = ord(getkey())

    #             0    1    2    3
    if   a == [  27,   0,   0,   0 ]: tecla = 'Esc'    # Doble pulsación
    elif a == [  79,  80,   0,   0 ]: tecla = 'F1'
    elif a == [  79,  81,   0,   0 ]: tecla = 'F2'
    elif a == [  79,  82,   0,   0 ]: tecla = 'F3'
    elif a == [  79,  83,   0,   0 ]: tecla = 'F4'
    elif a == [  91,  49,  53, 126 ]: tecla = 'F5'
    elif a == [  91,  49,  55, 126 ]: tecla = 'F6'
    elif a == [  91,  49,  56, 126 ]: tecla = 'F7'
    elif a == [  91,  49,  57, 126 ]: tecla = 'F8'
    elif a == [  91,  50,  48, 126 ]: tecla = 'F9'
    elif a == [  91,  50,  49, 126 ]: tecla = 'F10'
    elif a == [  91,  50,  51, 126 ]: tecla = 'F11'
    elif a == [  91,  50,  52, 126 ]: tecla = 'F12'
    elif a == [  91,  50, 126,   0 ]: tecla = 'Ins'
    elif a == [  91,  51, 126,   0 ]: tecla = 'Del'
    elif a == [  79,  72,   0,   0 ]: tecla = 'Home'
    elif a == [  79,  70,   0,   0 ]: tecla = 'End'
    elif a == [  91,  49, 126,   0 ]: tecla = 'Home'   # Keypad numérico
    elif a == [  91,  52, 126,   0 ]: tecla = 'End'    # Keypad numérico
    elif a == [  91,  53, 126,   0 ]: tecla = 'PgUp'
    elif a == [  91,  54, 126,   0 ]: tecla = 'PgDn'
    elif a == [  91,  65,   0,   0 ]: tecla = 'Up'
    elif a == [  91,  66,   0,   0 ]: tecla = 'Down'
    elif a == [  91,  68,   0,   0 ]: tecla = 'Left'
    elif a == [  91,  67,   0,   0 ]: tecla = 'Right'
    elif a == [  91,  69,   0,   0 ]: tecla = 'Center' # Keypad numérico
    else:                             tecla = chr(a[0])

    return tecla

# -----------------------------------------------------------------------------

def inp(mensaje='', longitud=40, permitidas=IMPRIMIBLES, sep=''):

    'Retorna una cadena tecleada por el usuario.'

    if mensaje: out(mensaje)

    dato = ''

    fin = False

    while not fin:

        tecla = getkey()

        if tecla == CR:
            fin = True
        elif tecla == TAB or tecla == sep:
            fin = True
        elif tecla == ESC:
            t = esc()
            if len(t)>1:
                if t == 'Esc':
                    borrar(len(dato)) # Doble pulsación
                    dato = ''
            else:
                if len(dato) < longitud:
                    dato += t
                    out(t)
        elif tecla == BKS:
            if dato:
                out('\b \b')
                dato = dato[:-1]
        elif tecla in permitidas:
            if len(dato) < longitud:
                dato += tecla
                out(tecla)

    return dato

# -----------------------------------------------------------------------------

try:

    # DOS, Windows
    import msvcrt, ctypes
    getkey = msvcrt.getch

    class _CursorInfo(ctypes.Structure):

        'Clase de apoyo para función "cursor".'

        _fields_ = [("size", ctypes.c_int), ("visible", ctypes.c_byte)]

except ImportError:

    # Se asume Unix
    import tty, termios

    def getkey():

        'Retorna una tecla presionada.'

        file = sys.stdin.fileno()
        mode = termios.tcgetattr(file)

        try:
            tty.setraw(file, termios.TCSANOW)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(file, termios.TCSANOW, mode)

        return ch

# -----------------------------------------------------------------------------

def cursor(mostrar=True):

    'Oculta o muestra el cursor en pantalla.'

    if os.name == 'nt':
        ci = _CursorInfo()
        handle = ctypes.windll.kernel32.GetStdHandle(-11)
        ctypes.windll.kernel32.GetConsoleCursorInfo(handle, ctypes.byref(ci))
        ci.visible = mostrar
        ctypes.windll.kernel32.SetConsoleCursorInfo(handle, ctypes.byref(ci))
    elif os.name == 'posix':
        out(CSON if mostrar else CSOF)

# -----------------------------------------------------------------------------

def ejecutar(c):

    'Ejecuta comando de consola y retorna su salida en forma de lista.'

    p = Popen(c,shell=True,stdin=PIPE,stdout=PIPE,stderr=STDOUT,close_fds=True)

    return p.stdout.read().split('\n')[:-1]

# -----------------------------------------------------------------------------

def columnas(lista, cols, sep=None):

    'Dada una lista de cadenas, retorna una lista de columnas.'

    tabla = []

    if lista and cols:
        indice = 0
        for item in lista:
            tabla.append([])
            fila = item.split(sep)
            if fila:
                for num in cols:
                    tabla[indice].append(fila[num])
                indice += 1

    return tabla

#------------------------------------------------------------------------------

def name():

    'Retorna el nombre del programa.'

    cadena = sys.argv[0].split(os.sep)[-1] # os.sep: Separador ("\" o "/")

    if not cadena: cadena = 'main'

    return cadena

#------------------------------------------------------------------------------

def cat(*items):

    'Retorna una cadena formada por todos los items separados por espacios.'

    cadena = ''

    for item in items:
        if eslista(item):
            cadena += ','.join(item) + ' '
        else:
            cadena += str(item) + ' '

    return cadena.strip()

#------------------------------------------------------------------------------

def tupla(lista, comillas=False):

    'Retorna una cadena en formato tupla con comillas opcionales (para SQL).'

    cadena = ''

    if lista:

        cadena = '('

        for item in lista:
            if comillas and escadena(item):
                item = '"' + item.replace('"','""') + '"' # Replace para SQL.
            else:
                item = str(item)
            cadena += item + ','

        cadena = cadena[:-1] + ')'

    return cadena

#------------------------------------------------------------------------------
# Procedimientos
# -----------------------------------------------------------------------------

def guardar(lista, nombre):

    'Guarda una lista en un archivo.'

    archivo = open(nombre, 'w')

    for item in lista:
        # archivo.writelines() no agrega 'newline' (LF)
        archivo.write(item + LF)

    archivo.close()

# -----------------------------------------------------------------------------

def borrarsiexiste(archivo):

    'Borra un archivo si existe.'

    if os.path.isfile(archivo): os.remove(archivo)

# -----------------------------------------------------------------------------

def crearcarpeta(direccion):

    'Crea una carpeta si no existe.'

    if not os.path.exists(direccion): os.makedirs(direccion)

# -----------------------------------------------------------------------------

def clear(l=100):

    'Borra la pantalla de texto.'

    if os.name == 'posix':
        # Unix, Linux, MacOS, BSD, etc.
        os.system('clear')
    elif os.name in ('nt', 'dos', 'ce'):
        # DOS, Windows
        os.system('CLS')
    else:
        # Otros sistemas operativos
        print LF * l

# -----------------------------------------------------------------------------

def out(cadena):

    'Envía una cadena a stdout y limpia el buffer para que se imprima rápido.'

    sys.stdout.write(cadena)
    sys.stdout.flush()

# -----------------------------------------------------------------------------

def borrar(cantidad):

    'Borra hacia atrás la cantidad de caracteres especificada.'

    out('\b \b'*cantidad)

# -----------------------------------------------------------------------------

def borrarlinea():

    'Borra la línea actual en la pantalla.'

    out(ESC_CLRLINE+ESC_CSRHOME)

# -----------------------------------------------------------------------------

def mostrar(objeto, ordenar=False, sep=' : '):

    'Imprime ordenadamente el contenido de un diccionario o una lista.'

    if tipo(objeto) == 'dict':
        claves = objeto.keys()
        claves.sort()
        esp = ancho(claves)
        for item in claves:
            print item + espacios(esp-len(item)) + sep + str(objeto[item])
    elif tipo(objeto) == 'list':
        if ordenar: objeto.sort()
        esp = len(str(len(objeto)))
        for i in range(len(objeto)):
            item = objeto[i]
            print espacios(esp-len(str(i))) + str(i) + sep + str(item)
    else:
        print objeto

# -----------------------------------------------------------------------------

def cambiarcarpeta(direccion):

    'Cambia la carpeta de trabajo actual.'

    os.chdir(direccion)

#------------------------------------------------------------------------------

def error(mensaje):

    'Imprime un mensaje de error.'

    if eslista(mensaje):
        for item in mensaje: print item # Ej: sys.exc_info() en try/except
    else:
        print mensaje

# -----------------------------------------------------------------------------

def salir(estado=0):

    'Finaliza el programa.'

    sys.exit(estado) # 0=Successful, 1=Abnormal, 2=Syntax

# -----------------------------------------------------------------------------
# Clases
# -----------------------------------------------------------------------------

class Base:

    'Gestiona el uso de bases de datos relacionales en formato SQLite.'

    #--------------------------------------------------------------------------
    # Constructor
    #--------------------------------------------------------------------------

    def __init__(self, nombre='temp.db'):

        'Constructor. Crea o se conecta al archivo de base de datos.'

        self.conexion = sqlite3.connect(nombre)
        self.conexion.row_factory = sqlite3.Row # Nombre de campo como índice

    #--------------------------------------------------------------------------
    # Representador string
    #--------------------------------------------------------------------------

    def __str__(self):

        'Representación del objeto en formato cadena.'

        sql = """
              SELECT name FROM sqlite_master
              WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%'
              UNION ALL
              SELECT name FROM sqlite_temp_master
              WHERE type IN ('table','view')
              ORDER BY 1
              """

        cadena = ''

        for linea in self.sql(sql).fetchall():
            for item in linea:
                cadena += item + '\n'

        return cadena[:-1]

    #--------------------------------------------------------------------------
    # Definiciones de otros métodos
    #--------------------------------------------------------------------------

    def tabla(self, nombre, columnas):

        'Crea una tabla en la base de datos.'

        sql = cat('CREATE TABLE IF NOT EXISTS', nombre, tupla(columnas))

        self.sql(sql)

    #--------------------------------------------------------------------------

    def consultar(self, tabla, condicion='', columnas='*', orden=''):

        'Retorna el resultado de una consulta con SELECT.'

        sql = cat('SELECT', columnas, 'FROM', tabla)

        if condicion: sql = cat(sql, 'WHERE', condicion)

        # Ejemplo: condicion = 'campo LIKE "%{}%"'.format(campo)

        if orden: sql = cat(sql, 'ORDER BY', orden)

        return self.sql(sql)

    #--------------------------------------------------------------------------

    def insertar(self, tabla, cols, vals):

        'Inserta una línea en una tabla de la base de datos.'

        sql = cat('INSERT INTO', tabla, tupla(cols), 'VALUES', tupla(vals, 1))

        self.sql(sql)

    #--------------------------------------------------------------------------

    def borrar(self, tabla, condicion):

        'Borra líneas de una tabla que cumplan una condición.'

        sql = cat('DELETE FROM', tabla, 'WHERE', condicion)

        self.sql(sql)

    #--------------------------------------------------------------------------

    def cambiar(self, tabla, col, val, cond):

        'Cambia el valor de una columna de una tabla que cumpla una condición.'

        sql = cat('UPDATE', tabla, 'SET', col, '=', val, 'WHERE', cond)

        self.sql(sql)

    #--------------------------------------------------------------------------

    def tirar(self, tabla):

        'Borra una tabla de la base de datos.'

        sql = cat('DROP TABLE IF EXISTS ', tabla)

        self.sql(sql)

    #--------------------------------------------------------------------------

    def contar(self, tabla):

        'Retorna la cantidad de filas en una tabla.'

        return self.consultar(tabla, cols='COUNT(*)').fetchone()[0]

    #--------------------------------------------------------------------------

    def mayor(self, tabla, columna):

        'Retorna el mayor valor de una columna en una tabla.'

        return self.consultar(tabla, cols='MAX(' + columna + ')').fetchone()[0]

    #--------------------------------------------------------------------------

    def existe(self, nombre):

        'Retorna verdadero si la tabla existe en la base de datos.'

        tabla     = 'sqlite_master'
        condicion = "type='table' AND name LIKE '" + nombre + "'"
        columnas  = 'name'

        return self.consultar(tabla, condicion, columnas).fetchone() != None

    #--------------------------------------------------------------------------

    def sql(self, cadena, cometer=False):

        'Ejecuta una sentencia SQL sobre la base de datos.'

        try:
            resultado = self.conexion.execute(cadena)
        except:
            out('Error en la sentencia SQL:\n' + cadena)
            error(sys.exc_info())
            sys.exit(1)
        else:
            if not cadena.lstrip().upper().startswith("SELECT"):
                if cometer: self.cometer()
            else:
                return resultado

    #--------------------------------------------------------------------------

    def cometer(self):

        'Ejecuta "commit" sobre la base de datos, para guardar los cambios.'

        self.conexion.commit()

    #--------------------------------------------------------------------------

    def cerrar(self):

        'Guarda los cambios y cierra la base de datos.'

        self.cometer()
        self.conexion.close()

    #--------------------------------------------------------------------------
    # Destructor
    #--------------------------------------------------------------------------

    def __del__(self):

        'Destructor. Guarda los cambios y cierra la base de datos.'

        self.cerrar()

#------------------------------------------------------------------------------

class Consola(cmd.Cmd):

    '''
    Cmd se usa mediante subclases y se añade comandos al definir métodos con
    nombres como 'do_x()', donde 'x' es el nombre del comando que se quiere
    agregar. En este ejemplo, los nuevos comandos de usuario son 'hist' (histo-
    -rial) y 'exit'. Se necesita 'do_shell()' si se quiere escapes '!' y
    'do_eof()' si el caracter de fin de archivo tiene que ser capturado. La do-
    -cumentación para el objeto Cmd no menciona que 'do_help()' imprimirá la
    cadena de documentación de 'do_x()' si no se puede encontrar 'help_x()'.
    Se agregó un 'do_help()' a medida para que el usuario pueda escribir 'help
    help'.
    '''

    # http://code.activestate.com/recipes/280500-console-built-with-cmd-object

    # -------------------------------------------------------------------------
    # Constructor
    # -------------------------------------------------------------------------

    def __init__(self, prompt='> ', intro="¡Bienvenido a " + name() + "!"):

        'Invoca al constructor de Cmd, define el prompt y el mensaje inicial.'

        cmd.Cmd.__init__(self)

        self.prompt = prompt
        self.intro  = intro  # Por defecto es None.

    # -------------------------------------------------------------------------
    # Definiciones de comandos
    # -------------------------------------------------------------------------

    def do_hist(self, args):

        'Imprime la lista de comandos que fueron ingresados.'

        for i in self._hist:
            print i

    #--------------------------------------------------------------------------

    def do_exit(self, args):

        'Sale del programa.'

        return -1

    #--------------------------------------------------------------------------

    def do_quit(self, args):

        'Sale del programa.'

        quit()

    # -------------------------------------------------------------------------
    # Definiciones de comandos de soporte a Cmd
    # -------------------------------------------------------------------------

    def do_eof(self, args):

        'Sale del programa con el caracter de fin de línea.'

        return self.do_exit(args)

    #--------------------------------------------------------------------------

    def do_shell(self, args):

        'Pasa un comando a la consola del sistema cuando empieza con "!"'

        os.system(args)

    #--------------------------------------------------------------------------

    def do_help(self, args):

        """
        Imprime ayuda sobre los comandos.
        'help' o '?' sin argumentos imprime la lista de comandos disponibles.
        'help <comando>' o '? <comando>' imprime ayuda sobre <comando>.
        """

        # Definimos este método para el texto de ayuda en la cadena doc.
        cmd.Cmd.do_help(self, args)

    # -------------------------------------------------------------------------
    # Métodos de invalidación en el objeto Cmd
    # -------------------------------------------------------------------------

    def preloop(self):

        'Inicialización antes de solicitar un comando al usuario.'

        cmd.Cmd.preloop(self)  # Prepara el completado de comandos.
        self._hist    = []     # Historial vacío.
        self._locals  = {}     # Inicializa el espacio de nombres de ejecución.
        self._globals = {}

    #--------------------------------------------------------------------------

    def postloop(self):

        'Finalización del ciclo de línea de comandos.'

        cmd.Cmd.postloop(self)  # Vacía el completado de comandos.

        print "Finalizando..."

    #--------------------------------------------------------------------------

    def precmd(self, line):

        """
        Este método se llama después de que la línea fue ingresada pero antes
        de que sea interpretada. Si desea modificar la línea entrada antes de
        su ejecución (por ejemplo, para sustituir una variable) hágalo aquí.
        """

        self._hist += [ line.strip() ]

        return line

    #--------------------------------------------------------------------------

    def postcmd(self, stop, line):

        """
        Para detener la consola, devolver algo que evalúa en 'True'.
        Si desea hacer algún tipo de procesamiento posterior, hágalo aquí.
        """

        return stop

    #--------------------------------------------------------------------------

    def emptyline(self):

        'Ignorar línea vacía.'

        pass

    #--------------------------------------------------------------------------

    def default(self, line):

        """
        Llamada en una línea de entrada cuando el prefijo no es reconocido.
        En ese caso, se ejecuta la línea como código Python.
        """

        try:
            exec(line) in self._locals, self._globals
        except Exception, e:
            print e.__class__, ":", e

# -----------------------------------------------------------------------------
# Este archivo puede importarse como módulo gracias a la siguiente sentencia:
# -----------------------------------------------------------------------------

if __name__ == '__main__':
    consola = Consola()
    consola.cmdloop()

# =============================================================================
