# Funciones, procedimientos y clases útiles:

## ARCHIVO
```
fun archivos(ruta='.', ext='')
pro borrarsiexiste(archivo)
pro cambiarcarpeta(direccion)
fun caminoabsoluto(direccion)
fun caminoactual()
fun carpetapersonal()
pro crearcarpeta(direccion)
fun esarchivo(cadena)
fun escarpeta(cadena)
fun existe(cadena)
fun fixpath(cadena)
fun guardar(lista, nombre)
fun leer(archivo)
fun listarcarpeta(direccion)
fun montajes()
cla Base(nombre=name()+'.db')
```
## CADENAS
```
fun ancho(lista)
pro borrar(cantidad)
pro borrarlinea()
pro clear(l=100)
fun columnas(lista, cols, sep=None)
fun escadena(objeto)
fun eslista(objeto)
fun esbinario(dato)
fun espacios(cantidad)
fun juntar(lista, sep='\\n')
fun limpiar(cadena)
fun llenar(cadena, cantidad, caracter='0', ajuste='d')
pro mostrar(objeto, ordenar=False)
pro out(cadena)
fun cat(*items)
fun tupla(lista, comillas=False)
```
## INGRESO
```
fun esc()
fun getkey()
fun inp(mensaje='', longitud=40, permitidas=IMPRIMIBLES, sep='')
cla Consola(prompt='> ', intro="¡Bienvenido a " + name() + "!")
```
## INTROSPECCION
```
fun doc(objeto)
fun name()
fun tipo(objeto)
```
## SISTEMA
```
fun arg(pos=False)
fun cursor(mostrar=True)
fun ejecutar(c)
fun fecha(epoch, corta=True)
fun fechas()
fun hostname()
fun xy()
pro error(mensaje)
pro salir(estado=0)
```
## LAMBDAS
```
miles : Devuelve una cadena con el número separado en miles.
esbin : Devuelve True si los bytes no son todos caracteres de texto.
veces : Devuelve una lista en base a un rango, de 1 al número especificado.
```